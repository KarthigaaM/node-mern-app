var seleniumServer = require("selenium-server");
var binPath = seleniumServer.path;

module.exports = {
  "src_folders": ["tests"],
  "output_folder": "reports",
  "custom_commands_path": "",
  "custom_assertions_path": "",
  "page_objects_path": "",
  "globals_path": "",

  "selenium": {
    "start_process": true,
    "server_path": "./bin/selenium-server/selenium-server-standalone-2.53.0.jar",
    "log_path": "",
    "port": 4445,
    "cli_args": {
      "webdriver.chrome.driver": "./bin/chrome-driver/chromedriver"
    }
  },

  "test_settings": {
    "default": {
      "launch_url": "http://localhost:3001/#/",
      "selenium_port": 4445,
      "selenium_host": "localhost",
      "silent": true,
      "screenshots": {
        "enabled": true,
        "path": "./screenshots"
      },
      "globals": {
        "userName": "",
        "password": ""
      },
      "desiredCapabilities": {
        "browserName": "chrome",
        "javascriptEnabled": true,
        "marionette": true
      }
    },

    "chrome": {
      "desiredCapabilities": {
        "browserName": "chrome",
        "javascriptEnabled": true,
        "acceptSslCerts": true
      }
    },
    "firefox": {
      "desiredCapabilities": {
        "browserName": "firefox",
        "marionette": true,
        "javascriptEnabled": true
      }
    }
  }
}
